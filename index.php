<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки

$data = [
'Eurasia' => [
			'Equus',          //лошадь
			'Ovis aries',      //овца
			'Sus scrofa',       //кабан
			'Canis lupus',       //волк
			'Alces',            //лось
			'Ursus maritimus',   //белый медведь
			'Panthera tigris',    //тигр
			'Lynx',              //рысь
			'Panthera uncia',       //снежный барс - ирбис
			'Ovis ammon',    //алтайский архар
			'Martes zibellina',    //соболь
			'Aquila chrysaetos',  //беркут
			'Pongo'              //орангутан
			], 
'Africa' => [
			'Loxodonta',               //слон
			'Hippopotamus amphibius',  //бегемот
			'Rhinocerotidae',          //носорог
			'Giraffa camelopardalis',  //жираф.
			'Panthera leo',             //лев
			'Hyaenidae',               //гиена
			'Hippotigris',                //зебра
			'Connochaetes',              //антилопа гну
			'Phoenicopterus roseus',     //фламинго
			'Chamaeleonidae',           //хамелеон
			'Machairodontinae'          //саблезубый тигр
			], 
'North America' => [
			'Castor canadensis', //бобр канадский
			'Alligator',     //аллигатор
			'Bison',          //бизон
			'Cervidae',          //олень
			'Erethizon dorsatum',     //дикобраз
			'Gymnogyps californianus', //кондор
			'Ovibos moschatus',    //овцебык
			'Mephitis',        //скунс
			'Arctodus simus',     //гигант-ий короткомордый медведь
			'Miracinonyx',     //гигант-ий гепард
			'Mammutidae',        //мастодонт
			'Mammuthus columbi'    //мамонт колумба
			], 
'South America' => [
			'Serrasalmus',         //пираньи
			'Chinchilla lanigera',  //шиншилла
			'Lama',              //лама
			'Puma concolor',      //пума
			'Eunectes murinus',    //анаконда
			'Vultur gryphus',      //андский кондор
			'Iguanidae',         //игуана
			'Smilodon',         //смилодон
			'Litopterna',        //литоптерн
			'Doedicurus',        //дидикурус
			'Megatherium'        //мегатерий
			], 
'Australia' => [
			'Macropus',                 //кенгуру
			'Ornithorhynchus anatinus',    //утконос
			'Tachyhlossidae',           //ехидна
			'Sarcophilus laniarius',   //тасманийский дьявол
			'Myrmecobius fsciatus',     //муравьед
			'Dromaius novaehollandiae',   //эму
			'Vombatidae',               //вомбаты
			'Diprotodon',           //дипротодон
			'Varanus',            //вараны
			'Wonambi'            //вонамби
			], 
'Antarctica' => [
			'Pygoscelis antarctica',   //пингвин
			'Hydrurga leptonyx',        //морской леопард
			'Leptonychotes weddellii',   //тюлень Уэддела
			'Mirounga',                 //морской слон
			'Thalassoica antarctica',   //буревестник антарктический
			'Belgica antarctica',      //бескрылый комар
			'Balaenoptera musculus',    //синий кит
			'Cryolophosaurus',        //криолофозавр
			'Ankylosauria',          //анкилозавр
			'Hadrosauridae'         //гадрозавр
			], 
'Pangea' => [
			'Placodermi',    //панцирная рыба
			'Apterygota',     //бескрылое насекомое
			'Ichthyostega',   //ихтиостега
			'Trilobita',      //трилобит
			'Eurypterida',    //морской скорпион
			'Myriapoda',          //многоножка
			'Plesiosiro madeleyi',  //
			'Seymouria',            //сеймурия
			'Stegocephalia',       //стегоцефал
			'Gymnophiona',       //
			'Anthracosauria'      //антракозавр
			]
];

$data_new = [];   //создаю новый массив, в который буду записывать животных со сложными именами
foreach ($data as $key => $data_contin) {
	foreach ($data_contin as $key_1 => $animal) {
	$word_animals = explode(" ", $animal); //разбиваю название на два слова
		if( count($word_animals) === 2 ) {     //если название животного содержит пробел
			$data_new[] = [
			$key,               //вношу в массив континент
			$word_animals[0],   //вношу в массив первое слово
			$word_animals[1]   //вношу в массив второе слово
			];
		}
	}
}

$x = range ( 0 , count($data_new) - 1 ); 
shuffle( $x ); 


$h2 = ''; 
// $animal_fantasy = ''; 
$animal_fantasy[] = ''; 
$point = ''; 
foreach ( $data_new as $key_1 => $animal ) {
	if ( $data_new[ $key_1 ][ 0 ] <> $h2 ) { 
		// echo $animal_fantasy . $point;    // фант. жив. 
		echo implode( ', ', $animal_fantasy ) . $point;   // фант. жив. 
		echo '<h2>' . $data_new[$key_1][0] . '</h2>';  //название текущего континента
		$h2 = $data_new[$key_1][0]; 
		// $animal_fantasy = $data_new[$key_1][1] . ' ' . $data_new[ $x[$key_1] ][2]; 
		$animal_fantasy = [  $data_new[$key_1][1] . ' ' . $data_new[ $x[$key_1] ][2]  ];
		$point = '. '; 
	} 
		else { 
			// $animal_fantasy = $animal_fantasy . ', ' . $data_new[$key_1][1] . ' ' . $data_new[ $x[$key_1] ][2]; 
			array_push( $animal_fantasy, $data_new[$key_1][1] . ' ' . $data_new[ $x[$key_1] ][2] ); 
			} 
}
// echo $animal_fantasy . $point;  // фант. жив. 
echo implode( ", ", $animal_fantasy ) . $point; 